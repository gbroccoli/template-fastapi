from alembic import command
from alembic.config import Config
from core.devforge import Msg
import click


def alembic_upgrade(alembic_cfg_path: str = "alembic.ini"):
    """Применяет все миграции до последней."""
    try:
        alembic_cfg = Config(alembic_cfg_path)
        command.upgrade(alembic_cfg, "head")
    except Exception as e:
        Msg.error("Error!", f"{e}")
    else:
        Msg.success("Success!", "Database updated!")


@click.command(name="db:apply")
def upgrade_migration():
    """Команда для применения миграций."""
    alembic_upgrade()
