from typing import Dict, List, Any, TypedDict, Optional

class RouteTypeItem(TypedDict):
    url: object
    prefix: str = Optional

RouteType = List[RouteTypeItem]
