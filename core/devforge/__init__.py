from .setting import ConfigDevForge # noqa
from .msg import Msg # noqa
from .command.secret.secretgenerate import keygenerate #s noqa
from .command.database.create_migration import create_migration # noqa
from .command.database.create_model_file import create_table # noqa
from .command.database.update_db import upgrade_migration # noqa
from .command.database.undo_db import downgrade


__version__ = "1.2.1"
