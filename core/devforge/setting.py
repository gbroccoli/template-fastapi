from typing import TypedDict, Literal


class Colos(TypedDict):
    error: str
    success: str
    warning: str
    info: str
    highlight: str
    default: str


class styleFont(TypedDict):
    bold: str
    italic: str
    underline: str
    strikethrough: str
    subscript: str
    superscript: str
    reset: str


class ConfigDevForge:

    def __init__(self) -> None:
        self.__dev_forge_version: str = "0.0.1"
        self.__colors: Colos = {
            "error": "\033[91m",  # Красный для ошибок
            "success": "\033[92m",  # Зелёный для успешных операций
            "warning": "\033[93m",  # Жёлтый для предупреждений
            "info": "\033[94m",  # Синий для информационных сообщений
            "highlight": "\033[95m",  # Пурпурный для выделения
            "default": "\033[0m",  # Сброс настроек цвета
        }
        self.__style_font: styleFont = {
            "bold": "\033[1m",  # ANSI escape code для жирного текста
            "italic": "\033[3m",  # ANSI escape code для курсива
            # ANSI escape code для подчеркнутого текста
            "underline": "\033[4m",
            # ANSI escape code для зачеркнутого текста
            "strikethrough": "\033[9m",
            "subscript": "\033[2m",  # ANSI escape code для подстрочного текста
            # ANSI escape code для надстрочного текста
            "superscript": "\033[6m",
            "reset": "\033[0m",  # ANSI escape code для сброса стиля
        }

    @property
    def colors(self) -> Colos:
        return self.__colors

    @property
    def style_font(self) -> styleFont:
        return self.__style_font

    def get_colors(self, color: Literal['error', 'success', 'warning', 'info', 'highlight', 'default']) -> str:
        return self.colors.get(color)

    def get_style_font(self, style: Literal['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript', 'reset']) -> str:
        return self.style_font.get(style)
