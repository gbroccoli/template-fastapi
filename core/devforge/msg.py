
from typing import TypedDict
from core.devforge.setting import ConfigDevForge


class Msg(ConfigDevForge):
    def __init__(self) -> None:
        super().__init__()

    # Функция для генерации сообщения об ошибке
    @classmethod
    def error(cls, title, msg=""):
        print(
            f"{cls().get_colors("error")}{cls().get_style_font('bold')}{title}{
                cls().get_style_font('reset')}{cls().get_colors('default')}\n"
            f"{msg}"
        )

        # Функция для генерации сообщения об успехе
    @classmethod
    def success(cls, title, msg=""):
        print(
            f"{cls().get_colors('success')}{cls().get_style_font('bold')}{title}{
                cls().get_style_font('reset')}{cls().get_colors('default')}\n"
            f"{msg}"
        )

        # Функция для генерации сообщения с информацией
    @classmethod
    def info(cls, title, msg=""):
        print(
            f"{cls().get_colors('info')}{cls().get_style_font('bold')}{title}{
                cls().get_style_font('reset')}{cls().get_colors('default')}\n"
            f"{msg}"
        )

        # Функция для генерации сообщения с предупреждением
    @classmethod
    def warning(cls, title, msg=""):
        print(
            f"{cls().get_colors('warning')}{cls().get_style_font('bold')}{title}{
                cls().get_style_font('reset')}{cls().get_colors('default')}\n"
            f"{msg}"
        )
