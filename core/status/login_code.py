from enum import Enum


class AuthorizationStatus(Enum):
    # Коды для авторизации
    AUTH_SUCCESS = "code_20_00"          				# Авторизация успешна
    INVALID_LOGIN = "code_20_01"         				# Неверный логин или пароль
    ACCOUNT_NOT_ACTIVATED = "code_20_02" 				# Аккаунт не активирован
    ACCOUNT_BLOCKED = "code_20_03"       				# Аккаунт заблокирован
    # Требуется двухфакторная аутентификация
    TWO_FACTOR_REQUIRED = "code_20_04"
    SESSION_EXPIRED = "code_20_05"       				# Сессия истекла
    # Непредвиденная ошибка при авторизации.
    UNEXPECTED_ERROR_AUTHENTICATION = "code_20_06"
