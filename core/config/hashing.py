from core.features.envconfig import EnvDataReader

app_hash = EnvDataReader()


rounds = app_hash.get_value("BCRYPT_ROUNDS", default=12)