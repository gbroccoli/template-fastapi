# db:undo
import click
from alembic import command
from alembic.config import Config
from core.devforge import Msg

def downground(count: str, alembic_cfg_path: str = "alembic.ini"):
    try:
        alembic_cfg = Config(alembic_cfg_path)
        command.downgrade(alembic_cfg, f"-{count}")
    except Exception as e:
       Msg.error('Error!', e)
    else:
        Msg.success('Success!', "Миграции успешно отозваны!")
    

@click.command(name="db:undo")
@click.option("--count", help="Количество отката", default="1", type=str)
def downgrade(count: str):
    """Команда для отката миграций"""
    downground(count)
