from core.models.urlModel import RouteType
from . import login
from . import register

urlpatterns: RouteType = [
    {"url": login.app_login},
    {"url": register.app_register}
]
