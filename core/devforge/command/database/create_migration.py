import click
from alembic import command
from alembic.config import Config
from core.devforge import Msg

def alembic_create_migration(message: str, alembic_cfg_path: str = "alembic.ini"):
    """Создает новую миграцию с заданным сообщением."""
    try:
        alembic_cfg = Config(alembic_cfg_path)
        command.revision(alembic_cfg, autogenerate=True, message=message)
        Msg.success("Success!", "Успешно созданная миграция!")
    except Exception as e:
        Msg.error("Ошибка", e)

@click.command(name="db:new-migration")
@click.option("--name", 'migration_name', help="Название миграции")
def create_migration(migration_name):
    """Команда для создания новой миграции."""

    alembic_create_migration(migration_name)
