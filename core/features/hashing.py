import bcrypt
from core.config.hashing import rounds


class PasswordManager:
    @staticmethod
    def hash_password(password):
        rounds = rounds

        salt = bcrypt.gensalt(rounds=rounds)
        hashed_password = bcrypt.hashpw(password.encode('utf-8'), salt)
        return hashed_password.decode('utf-8')

    @staticmethod
    def verify_password(input_password, hashed_password):
        return bcrypt.checkpw(
            input_password.encode('utf-8'),
            hashed_password.encode('utf-8')
        )
