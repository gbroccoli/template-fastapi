from alembic import command
from alembic.config import Config
from jinja2 import Template
from core.devforge import Msg
import inflect
import os
import click

p = inflect.engine()

model_template = """
from sqlalchemy import *
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
from core.config.database import Base

class {{class_name}}(Base):
	__tablename__ = '{{ table_name }}'
	id = Column(Integer, primary_key=True, autoincrement=True)
    # < name_params > = relationship("{{class_name}}")
	created_at = Column(DateTime(timezone=True), server_default=func.now())

__all__ = ['{{class_name}}']
"""


def create_model_file(table_name):
    path = os.path.join(os.getcwd(), "database", "models")

    class_name = p.plural(table_name).capitalize()

    table_name = p.plural(table_name).lower()

    template = Template(model_template)
    model_content = template.render(
        class_name=class_name, table_name=table_name)

    if not os.path.exists(path=path):
        os.makedirs(path)

    with open(os.path.join(path, table_name.lower() + ".py"), "w") as file:
        file.write(model_content)

    Msg.success("Success!", "Успешно создан файл модели!")


@click.command(name="db:new-model")
@click.option("--table", required=True, help="Имя таблицы")
def create_table(table):
    """Создает файл модели."""
    create_model_file(table)
