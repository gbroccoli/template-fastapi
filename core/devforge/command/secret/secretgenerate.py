import os
import base64
import secrets
from core.devforge import Msg
import click


def generate_secret_key():
    # Генерировать случайный байтовый ключ
    key = secrets.token_bytes(32)
    return base64.b64encode(key).decode('utf-8')


def create_or_load_secret_key():
    # Путь к файлу .env (на два уровня выше относительно исполняемого скрипта)
    env_file_path = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__),
            "..",
            "..",
            "..",
            "..",
            ".env"))
    secret_key = generate_secret_key()

    # Проверить, существует ли файл .env
    if os.path.isfile(env_file_path):
        with open(env_file_path, encoding='utf-8') as file:
            lines = file.readlines()

        with open(env_file_path, "w") as env_file:
            for line in lines:
                if line.startswith("APP_KEY="):
                    # Если строка начинается с "APP_KEY=", заменить значение на
                    # сгенерированный секретный ключ
                    env_file.write(f"APP_KEY={secret_key}\n")
                else:
                    # В противном случае, оставить строку без изменений
                    env_file.write(line)
        Msg.success("Success!", "Ключ успешно сгенерировался")
    else:
        # Если файла .env не существует, выдать ошибку
        Msg.error(title="Ошибка!", msg="Файл.env не существует")

    return secret_key


@click.command(name="key:generate", help="Сгенерировать секретный ключ")
def keygenerate():
    """Функция для создания секретного ключа"""
    create_or_load_secret_key()
