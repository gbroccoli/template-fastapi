from fastapi import FastAPI
import uvicorn
from fastapi.responses import FileResponse
from fastapi_csrf_protect import CsrfProtect
from fastapi_csrf_protect.exceptions import CsrfProtectError
from routers import urlpatterns
from pydantic import BaseModel

app = FastAPI()

# class CsrfSettings(BaseModel):
#     secret_key: str = "asecrettoeverybody"
#     cookie_samesite: str = "none"

# @CsrfProtect.load_config
# def get_config_csrf():
#     return CsrfSettings()


@app.get('/')
async def index():
    return FileResponse("tmp/index.html")

if urlpatterns:
    for url in urlpatterns:
        app.include_router(url.get("url"))
else:
    print("Нету API!")

def main():
    uvicorn.run(
        "main:app",
        port=8000,
        log_level="info",
        reload=True, # перед релизом поставить False или удалить данную строку
        workers=4
    )


if __name__ == '__main__':
    main()
