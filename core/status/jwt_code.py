from enum import Enum


class JWTStatus(Enum):
    JWT_CREATED = "code_50_00"         					# JWT успешно создан
    JWT_EXPIRED = "code_50_01"         					# JWT истек
    JWT_INVALID = "code_50_02"         					# Недействительный JWT
    JWT_VALID = "code_50_03"           					# JWT действителен
    JWT_REFRESH_REQUIRED = "code_50_04"  				# Требуется обновление JWT
    JWT_SIGNATURE_FAILED = "code_50_05"  				# Ошибка подписи JWT
    JWT_MISSING = "code_50_06"         					# JWT отсутствует
    JWT_DECODE_ERROR = "code_50_07"    					# Ошибка декодирования JWT
    JWT_INVALID_AUDIENCE = "code_50_08"					# Недействительная аудитория JWT
    JWT_INVALID_ISSUER = "code_50_09"  					# Недействительный издатель JWT
    JWT_ACCESS_DENIED = "code_50_10"   					# Доступ запрещен на основе JWT