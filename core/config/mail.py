from typing import TypedDict, Union
from core.features.envconfig import EnvDataReader

env = EnvDataReader()


class Email(TypedDict):
    mail_host: str
    smtp_port: Union[int, str]
    mail_username: str
    mail_password: str
    mail_encryption: Union[str, bool]
    mail_from_address: str
    mail_from_name: str


def get_mail_config() -> Email:
    return {
        "mail_host": env.get_value('MAIL_HOST', default='127.0.0.1'),
        "smtp_port": env.get_value('MAIL_PORT', default=465),
        "mail_username": env.get_value('MAIL_USERNAME'),
        "mail_password": env.get_value('MAIL_PASSWORD'),
        "mail_encryption": env.get_value('MAIL_ENCRYPTION', default=False),
        "mail_from_address": env.get_value('MAIL_FROM_ADDRESS'),
        "mail_from_name": env.get_value('MAIL_FROM_NAME')
    }