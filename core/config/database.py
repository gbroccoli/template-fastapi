from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker, DeclarativeBase
from core.features.envconfig import EnvDataReader
from typing import TypedDict, Union


class DBConfig(TypedDict):
    host: str
    user: str
    password: str
    database: str
    port: Union[str, int]


envDB = EnvDataReader()


class Base(DeclarativeBase):
    pass


DB_CONFIG: DBConfig = {
    "host": envDB.get_value("DB_HOST", default="localhost"),
    "user": envDB.get_value("DB_USERNAME"),
    "password": envDB.get_value("DB_PASSWORD"),
    "database": envDB.get_value("DB_DATABASE"),
    "port": envDB.get_value("DB_PORT", default=5432),
}


DB_URL = f"postgresql+asyncpg://{DB_CONFIG.get('user')}:{DB_CONFIG.get('password')}@{
    DB_CONFIG.get('host')}:{DB_CONFIG.get('port')}/{DB_CONFIG.get('database')}"

engine = create_async_engine(DB_URL)

async_session_maker = sessionmaker(
    engine,
    class_=AsyncSession,
    expire_on_commit=False
)
