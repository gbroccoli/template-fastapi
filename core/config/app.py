from core.features.envconfig import EnvDataReader
from typing import TypedDict

app_envs = EnvDataReader()

app_name = app_envs.get_value('APP_NAME', default="FastApi")
app_env = app_envs.get_value('APP_ENV', default="local")
key = app_envs.get_value('APP_KEY', default="")
app_timezone = app_envs.get_value('APP_TIMEZONE', default="UTC")
app_algorithm = app_envs.get_value('APP_ALGORITHM', default="HS256")