from enum import Enum


class AccessRights(Enum):
    ACCESS_DENIED = "code_40_00"  						# Доступ запрещен
    ADMIN_ACCESS = "code_40_01"   						# Доступ администратора
    USER_ACCESS = "code_40_02"    						# Доступ пользователя
    GUEST_ACCESS = "code_40_03"   						# Доступ гостя
    EDITOR_ACCESS = "code_40_04"  						# Доступ редактора
    MODERATOR_ACCESS = "code_40_05"  					# Доступ модератора
    READ_ONLY_ACCESS = "code_40_06"  					# Доступ только для чтения
    FULL_ACCESS = "code_40_07"    						# Полный доступ
    NO_ACCESS = "code_40_08"      						# Нет доступа
    LIMITED_ACCESS = "code_40_09" 						# Ограниченный доступ