from enum import Enum


class LogoutStatus(Enum):
	# Коды статуса для операции выхода из системы (logout)
	LOGOUT_SUCCESS = "code_30_00"          				# Выход из системы успешно выполнен
	NO_ACTIVE_SESSION = "code_30_01"       				# Не найдена активная сессия для завершения
	SESSION_TERMINATION_FAILED = "code_30_02" 			# Ошибка при попытке завершить сессию
	UNAUTHORIZED_ACCESS = "code_30_03"     				# Попытка выхода без авторизации
	UNEXPECTED_ERROR_LOGOUT = "code_30_04" 				# Непредвиденная ошибка при попытке выхода из системы