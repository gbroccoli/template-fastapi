from enum import Enum


class RegistrationStatus(Enum):
    # Коды для регистрации
    REGISTRATION_SUCCESS = "code_10_00"  				# Регистрация успешна
    USER_ALREADY_EXISTS = "code_10_01"   				# Пользователь уже существует
    INVALID_EMAIL_FORMAT = "code_10_02"  				# Недопустимый формат электронной почты
    # Пароль не соответствует требованиям безопасности
    WEAK_PASSWORD = "code_10_03"
    MISSING_FIELDS = "code_10_04"        				# Не заполнены обязательные поля
    # Ошибка подтверждения электронной почты
    EMAIL_VERIFICATION_ERROR = "code_10_05"
    # Непредвиденная ошибка при регистрации.
    UNEXPECTED_ERROR_REGISTRATION = "code_10_06"
